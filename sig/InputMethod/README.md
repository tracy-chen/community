## 输入法SIG组

### 工作目标

本SIG组致力于组建输入法框架以及输入法开源社区,推进输入法框架以及输入法在社区维护

### SIG成员
- hanteng@kylinos.cn
- liulinsong@kylinos.cn
- linyuxuan@kylinos.cn
- zhaokexin@kylinos.cn

### SIG维护包列表
- fcitx
- fcitx-qt5
- fcitx-configtool
- im-config
- fcitx5
- fcitx5-qt
- fcitx5-gtk
- fcitx5-configtool
- fcitx5-chinese-addons
- fcitx5-rime
- kylin-virtual-keyboard 


# RTHypervisor

openKylin RTHypervisor小组致力于实时虚拟化技术的研究，目前主要包括Jailhouse。提供工控、车载等领域实时控制的虚拟化解决方案。

## 工作目标

- 提供工控、车载等领域实时场景的虚拟化解决方案
- 负责Jailhouse技术在主要硬件平台的适配和运行
- 增加Jailhouse在实时性、安全性及稳定性等方面的新特性和新功能
- 关注其它前沿的实时虚拟化技术，共同推进实时虚拟化的发展

## 邮件列表



## SIG成员

### Owner

- [zhangyunfei@kylinos.cn](https://gitee.com/yuncomputing)

### Maintainers

- [huanglei1@kylinos.cn](https://gitee.com/leihwang)
- [chenyangping@kylinos.cn](https://gitee.com/tracy-chen)
- [cuiyanzhao@kylinos.cn](https://gitee.com/cuiyanzhao)
- [mashuai01@kylinos.cn](https://gitee.com/mashuai1996)
- [cuijianying@kylinos.cn](https://gitee.com/cc380)
- [electricpower_gao@163.com](https://gitee.com/chengbo-gao)
- [zhangyoujing@kylinos.cn](https://gitee.com/zhangyoujing666)

### Committers

## 邮件列表
- rthypervisor@lists.openkylin.top


## 仓库列表
- [jailhouse](https://gitee.com/openkylin/jailhouse)

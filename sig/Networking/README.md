##  网络管理组

### 工作目标

优化openKylin网络连接管理，提升网络性能，完善网络管理功能。

## SIG成员

### Owner

- 赵阳  [zhaoyang@kylinos.cn](zhaoyang@kylinos.cn)

### Maintainers

- 赵阳  [zhaoyang@kylinos.cn](zhaoyang@kylinos.cn)

## SIG邮件列表

- [networking@list.openkylin.top](networking@list.openkylin.top)

## SIG维护包列表

- network-manager
- wpasupplicnat
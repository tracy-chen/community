## PhytiumKernelPatch SIG

PKP (PhytiumKernelPatch) is delicated to the development and maintenance of Phytium kernel patch.

## Goal-setting

1. Maintain the Phytium kernel patch code submitted by the community.
2. Improve the Phytium server kernel and synchronize the latest patch code.
3. Push the latest Phytium X100 kernel patch code.

## SIG Member

## Owner
Hongbo Mao (`maohongbo@phytium.com.cn`)

## Maintainers
Jiakun Shuai (`shuaijiakun1288@phytium.com.cn`)

## SIG Maintenance Packages List

- s2500-gic
- smmu-support
- kdump-fix
- interrupt-fix

## Mailing List
phytiumkernelpatch@lists.openkylin.top
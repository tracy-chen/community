## PhytiumKernelPatch SIG小组

PKP (PhytiumKernelPatch) SIG小组致力于支持飞腾特性的内核补丁开发与维护。

## 工作目标

1. 维护社区已提交的飞腾内核补丁代码。
2. 完善飞腾服务器内核并同步最新补丁代码。
3. 推送飞腾X100内核补丁代码。

## SIG成员

## Owner
毛泓博 (`maohongbo@phytium.com.cn`)

## Maintainers
帅家坤 (`shuaijiakun1288@phytium.com.cn`)

## SIG维护包列表

- s2500-gic
- smmu-support
- kdump-fix
- interrupt-fix

## SIG邮件列表
phytiumkernelpatch@lists.openkylin.top
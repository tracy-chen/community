# Framework兴趣小组（SIG）

Framework SIG致力于为openKylin社区提供集程序编辑、编译、调试、发布、分析等全套开发功能的编程环境，涵盖通用集成开发环境、编译工具链、运行时环境、类库等。SIG初期重点研制高效率、跨平台、插件化、易调试的通用集成开发环境，支持C、C++、Java、Go、Fortran、Python、JavaScript等多种标准编程语言，涵盖编码、编译、调试、性能分析、软件交付等一整套开发流程，满足openKylin平台上软件开发需求。


## 工作目标

- 研制OpenKylin集成开发环境，包含程序编辑、编译、调试、发布、分析等开发功能。
- 研制基于openKylin平台的编译工具，高级语言运行时环境等组件。


## repository

- [vscode-create-project](https://gitee.com/openkylin/vscode-create-project)
- [kylin-code](https://gitee.com/openkylin/kylin-code)

## SIG成员
### Maintainers
- wuchunguang(wuchunguang@kylinos.cn)
- liangkeming(liangkeming@kylinos.cn)
- mu_ruichao(muruichao@kylinos.cn)
- xw1985(xuewei@kylinos.cn)
- zhangyun-2022(zhangyun@kylinos.cn)
- chriswang521(wangpenglong@kylinos.cn)
- linchaochao(linchaochao@kylinos.cn)
- quanzhuo(quanzhuo@kylinos.cn)
- chenhaoyang(chenhaoyang@kylinos.cn)
### Committers


## 邮件列表


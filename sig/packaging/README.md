# 软件包管理 SIG

负责维护openKylin社区的软件包打包规范，维护公共软件包，以及协调和决策社区版本发布过程中的包依赖问题。

## SIG 职责和目标

- 制定、发布和维护社区的打包规范、打包原则和依赖规范
- 执行技术委员会的包引入和退出机制，预审软件包进入社区发布版本的申请
- 维护和管理公共软件包
- 协调和发布openKylin社区版本的软件包清单，与sig-release、sig-QA团队共同支撑社区版本的发布过程
- 协调各SIG组的软件包划分、依赖冲突等
- 建立和维护社区的包管理工具框架，支撑社区包管理工作的自动化和可溯源
- 维护版本发布件中的软件包基线列表

## 仓库

### SIG Owner
- maozhou

## SIG 成员
- xiewei
- zhouganqing2021
- luoyaoming

## 邮件列表
packaging@lists.openkylin.top

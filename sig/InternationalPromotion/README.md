# InternationalPromotion SIG

负责openKylin操作系统的国际化推广，开展国际交流会议，提升国产操作系统国际影响力，致力于世界各地爱好者加入openKylin社区建设。

## 工作目标

- 加强openKylin操作系统国际化交流合作
- 负责开展社区国际化推广活动
- 收集并解决国际用户使用反馈
- 维护openKylin社区国际化建设
- 提高社区平台国际活跃用户数等

## SIG成员

### owner

- wenzhuw
- guifei
- tianshuyang

### maintainer

- yu-junxia
- weii-pan

## 邮件列表
internationalpromotion@lists.openkylin.top
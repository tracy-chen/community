# 图标主题

  在制作图标主题包时可以参考 [openkylin-theme](https://gitee.com/openkylin/openkylin-theme)主题包中的 icons/ukui-icon-theme`*` 部分目录中的内容。

  在参与制作一个全新的主题包前，不妨先从已有的主题包维护入手，这样更有助于你对系统主题包的了解。

  首先在修改图标主题中的图片时，请遵守对图标命名的规范[图标规范请参考这里](https://docs.qq.com/sheet/DTkRyUm5wVkVab1pN?tab=BB08J2&u=9487ce2cd5cb4d2cb525bda876ff13f7)，最基础的就是不向文件夹里乱添加图片。为了使用户在切换主题时每个图标都能生效，这是必须的。

  当你打开图片文件夹时，你可以看见很多按照规格排列的文件夹。是的，这就是图片大小，当你得到一张.png切符合命名规范的图片想要替换到图标主题时，也请根据尺寸到相应文件夹以及相应类型下进行替换。请注意，scalable目录下没有固定尺寸，因为此文件夹下的图片为.svg 类型，.svg类型图片没有固定大小，颜色也可以根据程序进行改变。

  目录下的index.theme文件是配置文件。如果你检查到明明对应文件夹里有图片却一直没有显示或者显示尺寸有错，那么可能是这里出错了，每个目录下的文件夹都会被此文件配置，如果有新增加的文件夹那么请一定要更新此配置文件，不然此文件夹下的所有内容将不会被调用。

  那么到底要如何替换图标呢？截止目前为止，此目录下有170+的文件夹，有超过2.7万张图片，寻找某一张图片的无疑是大海捞针。方法一，在/usr/share/application目录下有对应的.desktop文件，打开对应软件的.desktop文件，可以看见Icon一行，后面接的就是对应软件名字；方法二，清楚需要替换的图片是属于什么类型，直接在对应类型的文件夹下寻找；这两种方法基本可以帮助你定位出错图片位置，当无法确认对应文件类型时就用排除法，删掉文件夹，如果被影响了，就说明图片在此文件夹下。

  在图标主题下有很多相同的图标，那么我们该如何控制他们同时切换呢，这个时候就用到了软链接，因软链接生成的图片会跟着父级图片进行改变。

  关于图标命名规范，图标名中不可以出现空格，不可以出现奇怪的符号，图片的权限也必须合理等等，你可以通过编包，跑lintian来检查错误，

# 第三方图标规范：

  根据freedesktop中描述，
> In order to have a place for third party applications to install their icons there should always exist a theme called "hicolor".

  为了给第三方应用程序提供一个安装图标的位置，应该始终存在一个名为“hicolor”的主题。

  根据freedesktop的继承关系中描述，
> If no theme is specified implementations are required to add the "hicolor" theme to the inheritance tree.

  如果没有指定主题，则要求实现者将 "hicolor "主题添加到继承树中。当我们的自研应用存在于其他桌面环境时，应作为第三方应用>，将我们的图标应该放在hicolor主题里。

> So, you're an application author, and want to install application icons so that they work in the KDE and Gnome menus. Minimally you should install a 48x48 icon in the hicolor theme. This means installing a PNG file in $prefix/share/icons/hicolor/48x48/apps.

  作为应用程序的开发者，为了在当前桌面环境菜单中正常显示，我们至少需要在hicolor中安装48x48尺寸的图标。$prefix/**share/icons/hicolor/48x48/apps**。为了更好的将图标显示（规避显示模糊的风险）以及表达我们的设计语言，请将svg图标，安装在$prefix/**share/icons/hicolor/scalable/** 下。程序内所需要的其他图标以及线性图标，同样的，根据设计提供的图标尺寸放在hicolor中相应的尺寸目录下。

freedesktop规范原文链接：https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html#icon_lookup

# 文档小组（SIG）

文档 SIG小组致力于创建各类文档方便大家的使用。


## 工作目标

- 帮助新人和开发更好的使用，开发openKylin，撰写包括但不限于使用文档，开发文档，各类教程等。

## SIG成员
- moshengrenx
- chipo
- delong1998
- AICloudOpser

### Owner
- moshengrenx

### Maintainer
- moshengrenx
- chipo
- AICloudOpser
- delong1998

## SIG维护包列表
- [docs](https://gitee.com/openkylin/docs)
- [sig-documentation](https://gitee.com/openkylin/sig-documentation)

## 邮件列表
- docs@lists.openkylin.top


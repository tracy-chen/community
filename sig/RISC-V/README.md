# RISC-V SIG

负责RISC-V开源软件包的维护。发布openKylin的RISC-V 版本，进行软件包构建、系统构建等工作。

## 工作目标

- 负责 openKylin RISC-V 版本的规划、制作、维护和升级
- 负责RISC-V同x86、ARM间的应用兼容技术探索

## 邮件列表

risc-v@lists.openkylin.top

## SIG成员

### Owner

- [wenzhuw](https://gitee.com/wenzhuw)
- [fighting-liu](https://gitee.com/fighting-liu)
- [handsome_feng](https://gitee.com/handsome_feng)

### Maintainers

- lizhuoheng@kylinos.cn
- dingchenguang@kylinos.cn
- cp0613@linux.alibaba.com
- ra.zhang@hl-it.cn

### Committers

## 仓库列表
- [rvtrans](https://gitee.com/openkylin/rvtrans)
- [riscv](https://gitee.com/openkylin/riscv)

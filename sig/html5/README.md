---
title: HTML5 SIG组章程
description: 
published: true
date: 2022-05-17T07:16:11.396Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:16:34.035Z
---

## HTML5兴趣小组（SIG）

HTML5 SIG专注于打造基于Web（HTML5、CSS、Javascript、WASM等）技术的终端操作系统桌面环境，负责相关代码软件仓库的规划、版本维护和升级工作。

## 工作目标

- 负责基于HTML5技术打造轻量级移动终端操作系统桌面环境
- 负责构建Web移动应用运行基础环境
- 负责相关以Web技术为基础的移动应用的维护

## SIG成员

## SIG members
### Owner
- Long Peng(penglong@yhkylin.cn)
- Ruobing Xia(xiaruobing@kylinos.cn)

### Maintainers
- Tao Mao(maotao@kylinos.cn)
- Wenjie Li(liwenjie@yhkylin.cn)
- Changqi Wang(wanghcangqi@kylinos.cn)
- Junkang Nong(njk_jackson@163.com)
- gzwpt lujun(lujun88@pku.org.cn)

## Email
html5@lists.openkylin.top

## SIG维护包列表

- gecko_b2g
- ura_homescreen
- manifests
- gaia_system
- gaia-buildinkeyborad
- star-web-components
- ura-components

## 启动与安装SIG组

### 工作目标

本SIG组致力于组建系统启动与系统安装,推进系统启动与系统安装在社区维护,应用社区相关新技术

### SIG成员
- liuyi@kylinos.cn
- liushanwen@kylinos.cn
- tangzhicheng@kylinos.cn
- yangdonghai@kylinos.cn

### SIG维护包列表
- gfxboot-theme-openkylin
- syslinux-themes-openkylin
- initramfs-tools
- casper
- kylin-os-installer

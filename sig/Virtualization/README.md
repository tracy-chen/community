# openKylin Virtualization

openKylin 社区是在开源、自愿、平等和协作的基础上，由基础软硬件企业、非营利性组织、社团组织、高等院校、科研机构和个人开发者共同创立的一个开源社区，致力于通过开源、开放的社区合作，构建桌面操作系统开源社区，推动Linux开源技术及其软硬件生态繁荣发展。

## 愿景

构建openKylin社区系统虚拟化技术，打造面向端、边、云的全场景虚拟化解决方案

## 职责

- 负责openKylin虚拟化相关组件社区技术发展和决策
- 负责openKylin虚拟化相关软件包的规划、升级和维护
- 及时响应openKylin虚拟化产品用户反馈和解决虚拟化相关问题

## 发展规划

### 22年底
- virtio-gpu 支持H264/H265硬件解码加速
- virtio-gpu 支持H264/H265硬件编码加速
- virtio-mem内存热插拔支持
- 针对小块文件读写性能优化后端io_uring加速
- 针对小块文件读写性能优化后端vhost-scsi加速

## SIG成员

### Owner
- 孙立明 sunliming@kylinos.cn
- 谢明 xieming@kylinos.cn

### Maintainers List
- [xieming-catcode](https://gitee.com/xieming-catcode)
- [liweishi12345](https://gitee.com/liweishi12345)
- [flynnjiang](https://gitee.com/flynnjiang)
- [billion_one](https://gitee.com/billion_one)
- [superw82](https://gitee.com/superw82)
- [longjun198806](https://gitee.com/longjun198806)
- [sunliming01](https://gitee.com/sunliming01)
- [powers-yujun](https://gitee.com/powers-yujun)
- [coreyliu](https://gitee.com/coreyliu)
- [hargrovee](https://gitee.com/hargrovee)
- [ocloudware_admin](https://gitee.com/ocloudware_admin)

### Contributors List
- [he_minhong](https://gitee.com/he_minhong)
- [ginnie_zhou](https://gitee.com/ginnie_zhou)
- [gehao](https://gitee.com/gehao)
- [crawler_zh](https://gitee.com/crawler_zh)
- [zeng_chi](https://gitee.com/zeng_chi)
- [caoxuhui](https://gitee.com/caoxuhui)
- [sharkct](https://gitee.com/sharkct)
- [xiajidong-ss](https://gitee.com/xiajidong-ss)
- [ocloudware_admin](https://gitee.com/ocloudware_admin)
- [bianbian](https://gitee.com/BianBian)

## 邮件列表
- Virtualization@lists.openkylin.top

## 项目清单
- [linux kernel](https://gitee.com/openkylin/linux)
- [qemu](https://gitee.com/openkylin/qemu)
- [libvirt](https://gitee.com/openkylin/libvirt)
- [edk2](https://gitee.com/openkylin/edk2)
- [virglrenderer](https://gitee.com/openkylin/virglrenderer)
- [spice](https://gitee.com/openkylin/spice)
- [mesa](https://gitee.com/openkylin/mesa)
- [virt-manager](https://gitee.com/openkylin/virt-manager)

## 开发指南
- [开发指南] (https://kdocs.cn/l/ciYm3CGNQJj8)

## 会议纪要
- [会议纪要] (https://kdocs.cn/l/cm3nUJ8iSDab)

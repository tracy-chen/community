# Release SIG

Release SIG 的目标是开发和发布管理，包括计划、需求、问题、风险等。

## SIG任务和范围

- 需求和功能管理

- 发布计划、开发计划

- 开发过程和风险管理

- 发布范围管理、发布说明

## 仓库 
- release-management
- all

## SIG 成员
### Owner
- 余杰
- 刘晓东
- 李剑峰

### 维护者
- maozhou
- handsome_feng
- xiewei
- allen-ukui
- shangxiaoyang
- kiber（zhangtianxiong@kylinos.cn）
- kylinzhangye1（zhangye1@kylinos.cn）
- 李天智
- 汪诗琴
- 毛晓裕


## 邮件列表
release@lists.openkylin.top

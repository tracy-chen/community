## 工作目标

QuarKAI SIG组（QuarkAI Special Interest Group，简称QuarkAI SIG） 随着开放麒麟操作系统的发布，我们QuarkAI SIG组主要负责开放麒麟的整个AI生态，AI平台开发。随着人工智能AI技术正不断前进，因此我们QuarkAI SIG组不忘初心，立志要将开放麒麟的AI生态，AI平台做大做强！


## SIG成员

- 张文涛(zwt-mail@qq.com)
- 陈朝臣(3174025065@qq.com) 

## SIG维护包列表

- QuarkAI
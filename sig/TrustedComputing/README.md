# 可信计算SIG（Trusted Computing SIG）

可信计算兴趣组（Trusted Computing SIG）主要目标是致力可信计算基础软件开源与优化，并提供可信计算相关的应用规范与接口，探索国内外可信计算技术的应用实践。

## 工作目标

1. 提供通用的可信计算基础软件实现，包括可信软件栈、基础可信执行环境，可信执行环境SDK
2. 实现可信基础软件的国密支持，丰富可信计算技术的应用场景，利用可信技术和可信执行环境增强openKylin社区产品的安全性
3. 追踪并研究业内前沿的可信技术、可信执行环境技术及各类标准规范
4. 将可信计算技术与openkylin通用安全相结合，增强系统整体安全性，提供对应的代码库，吸引广大的第三方软件/硬件参与者共建openKylin社区的可信计算技术

## SIG成员

### Owner
- 杨诏钧
- [岳佳圆](https://gitee.com/yuejiayuan)
- [于博](https://gitee.com/kylinyubo)

### Maintainers

- [zhangfan4ky](https://gitee.com/zhangfan4ky)
- [chenjinice](https://gitee.com/chenjinice)
- [wujiening](https://gitee.com/wujiening)
- [bian-xiuning](https://gitee.com/bian-xiuning)
- [corwin-song](https://gitee.com/corwin-song)
- [xieyuxing168](https://gitee.com/xieyuxing168)

## SIG维护包列表

- tpm2-abrmd
- tpm2-tss
- tpm2-tools
- optee-client

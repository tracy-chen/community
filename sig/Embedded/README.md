## Embedded-SIG组

本SIG组将打造openKylin嵌入式操作系统，提供常见工控总线、软实时、硬实时、虚拟化、web管理、轻量级、安全性等多种特性，繁荣openKylin系统生态。

## 工作目标

- 对openKylin系统进行裁剪，并形成嵌入式系统版本，并对该系统进行版本生命周期维护
- 依托openKylin集成构建平台，打造嵌入式自动化构建体系
- 为其他厂商发行开源嵌入式系统或商业系统提供技术支撑
- 研发针对嵌入式系统的SDK，并进行版本生命周期维护
- 提供适用于嵌入式场景的实时性方案
- 提供适用于嵌入式场景的轻量级UKUI桌面
- 提供嵌入式混合部署方案
- 引入Preempt_RT软实时方案
- 引入Xenomai 3实时性方案
- 引入Xenomai 4实时性方案
- 引入常见工控协议，如Libmodbus、CANOpen、CanFestival、IGH-EtherCAT
- 引入RTHypervisor SIG的jailhouse虚拟化方案
- 回馈上游社区，如kernel、Xenomai、jailhouse等

## ROADMAP

- 2023年2月，引入工控协议
- 2023年4月，针对飞腾开源内核增加RT补丁
- 2023年5月，针对飞腾开源内核增加Xenomai3实时方案
- 2023年6月，完成嵌入式系统与openKylin集成构造环境对接
- 2023年8月，针对飞腾开源内核增加Xenomai4实时方案
- 2023年9月，完成openKylin Embedded系统V1.0发布并开源
- 2023年10月，完成openKylin Embedded系统SDK发布并开源
- 2023年12月，完成openKylin Embedded系统安全增强功能

## SIG成员

### Owner

郭皓[@guohaocs2c](https://gitee.com/guohaocs2c) ([guohao@kylinos.cn](mailto:guohao@kylinos.cn))

### Maintainer列表

| Maintainer                                              | 邮箱                                                        |
| ------------------------------------------------------- | ----------------------------------------------------------- |
| 郭皓[@guohaocs2c](https://gitee.com/guohaocs2c)         | [guohao@kylinos.cn](mailto:guohao@kylinos.cn)               |
| 吴春光[@wuchunguang](https://gitee.com/wuchunguang)     | [wuchunguang@kylinos.cn](mailto:wuchunguang@kylinos.cn)     |
| 张培[@pzhanggit](https://gitee.com/pzhanggit)           | [zp_cn@126.com](mailto:zp_cn@126.com)                       |
| 张志成[@zzc-kylin](https://gitee.com/zzc-kylin)         | [zhangzhicheng@kylinos.cn](mailto:zhangzhicheng@kylinos.cn) |
| 黄顺玉[@huang-shunyu](https://gitee.com/huang-shunyu)   | [huangshunyu@kylinos.cn](mailto:huangshunyu@kylinos.cn)     |
| 陶术松[@tao12345](https://gitee.com/tao12345)           | [taoshusong@kylinos.cn](mailto:taoshusong@kylinos.cn)       |
| 马玉昆[@kylin-mayukun](https://gitee.com/kylin-mayukun) | [mayukun@kylinos.cn](mailto:mayukun@kylinos.cn)             |
| 李铭乐[@eelxela](https://gitee.com/eelxela)             | [limingle@kylinos.cn](mailto:limingle@kylinos.cn)           |
| 刘仁学[@liurenxue](https://gitee.com/liurenxue)         | [liurenxue@kylinos.cn](mailto:liurenxue@kylinos.cn)         |
| 张远航[@zhangyh1992](https://gitee.com/zhangyh1992)     | [zhangyuanhang@kylinos.cn](mailto:zhangyuanhang@kylinos.cn) |
| 张玉[@zhangyuge001](https://gitee.com/zhangyuge001)     | [zhangyu4@kylinos.cn](mailto:zhangyu4@kylinos.cn)           |
| 李钰磊[@r2018](https://gitee.com/r2018)                 | [liyulei@kylinos.cn](mailto:liyulei@kylinos.cn)             |
| 廖元垲[@liao-yuankai](https://gitee.com/liao-yuankai)   | [yuankai.liao@cdjrlc.com](mailto:yuankai.liao@cdjrlc.com)   |
| 廖茂益[@ixr](https://gitee.com/ixr)                     | [liaomaoyi@cdjrlc.com](mailto:liaomaoyi@cdjrlc.com)         |

### Committer列表

## SIG维护包列表

- [rk-kernel-5.4](https://gitee.com/openkylin/rk-kernel-5.4)
- [riscv-kernel-5.4](https://gitee.com/openkylin/riscv-kernel-5.4)
- [loongarch-kernel-5.4](https://gitee.com/openkylin/loongarch-kernel-5.4)
- [preempt_rt](https://gitee.com/openkylin/preempt_rt)
- [rtcheck](https://gitee.com/openkylin/rtcheck)
- [rteval](https://gitee.com/openkylin/rteval)
- [rt-tests](https://gitee.com/openkylin/rt-tests)
- [xenomai3](https://gitee.com/openkylin/xenomai3)
- [xenomai4](https://gitee.com/openkylin/xenomai4)
- [ipipe](https://gitee.com/openkylin/ipipe)
- [canfestival-xenomai](https://gitee.com/openkylin/canfestival-xenomai)
- [canfestival](https://gitee.com/openkylin/canfestival)
- [igh-ethercat-xenomai](https://gitee.com/openkylin/igh-ethercat-xenomai)
- [ethercat-igh](https://gitee.com/openkylin/ethercat-igh)
- [libmodbus-xenomai](https://gitee.com/openkylin/libmodbus-xenomai)
- [libmodbus](https://gitee.com/openkylin/libmodbus)
- [canopennode](https://gitee.com/openkylin/canopennode)
- [libnetconf2](https://gitee.com/openkylin/libnetconf2)
- [libyang1](https://gitee.com/openkylin/libyang1)
- [netopeer2](https://gitee.com/openkylin/netopeer2)
- [soem](https://gitee.com/openkylin/soem)
- [soes](https://gitee.com/openkylin/soes)
- [sysrepo](https://gitee.com/openkylin/sysrepo)

## 邮件列表

## SIG组例会


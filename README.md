# openKylin社区
[English](./README_en.md) | 简体中文

欢迎来到openKylin社区

## 介绍

代码仓 Community 保存了关于openKylin社区已创建的 SIG 组的相关信息。  
  
openKylin社区按照不同的SIG来组织，欢迎加入您感兴趣的SIG。  
  
SIG 即 Special Interest Group 的缩写，为了更好的管理和改善工作流程，openKylin社区按照不同的 SIG 来组织的，因此在进行社区贡献之前，需要先找到您感兴趣的 SIG。

## 原则

- 1、openKylin社区中所有的 SIG 小组都是开放的，任何人和组织都可以参与。

- 2、在 SIG 组的 README.md 文件中包含了该项目所属的SIG相关信息、交流方式、成员和联系方式等。我们欢迎大家通过 README.md 文件中提到的联系方式包括邮件列表、公开例会等途径积极参与进SIG内的交流。

- 3、每个 SIG 组都由项目核心成员：项目 Owner、审核者（Maintainer）和贡献者（Conrtibutor）组成，组内重大决策须由全体成员以超过2/3的票数投票决议并报备技术委员会。

- 4、每个 SIG 组都可以有自己的邮件列表、社群等，也可以有自己的贡献策略，但必须有自己的 SIG 章程。

- 5、社区各 SIG 组之间负责的事务类型不允许交叉，需要保持组之间的事务类型隔离。

- 6、各 SIG 组内会议需定期举行。
